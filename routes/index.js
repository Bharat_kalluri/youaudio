var express = require('express');
var router = express.Router();
var tools = require ('../controllers/youtube.js');

var fs = require('fs');
var ytdl = require('ytdl-core');

var bodyParser = require('body-parser');

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('index', { title: 'YouTube Audio' });
});

router.post('/',function(req,res){
  term = req.body.term;
  res.redirect('/search/'+term)
});

router.get('/search/:term',function(req,res) {
  term = req.params.term;
  urlis = tools.url_framer(term);
  tools.return_json(urlis)
    .then(function(result){
      result = JSON.parse(result)
      res.render('search_results.hbs',{result:result.items});
    })
});

router.get('/play/:id',function(req,res){
    urlis = 'https://www.youtube.com/watch?v='+req.params.id;
    stream = ytdl(urlis,{filter: 'audioonly'}).pipe(res);

    stream.on('data', function(data) {
      res.write(data);
    });

    stream.on('end', function() {
      res.end();
    });

});


module.exports = router;
